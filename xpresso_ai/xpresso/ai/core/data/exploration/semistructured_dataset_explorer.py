__all__ = ['SemiStructuredDatasetExplorer']
__author__ = 'Srijan Sharma'

from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    InvalidDatatypeException

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class SemiStructuredDatasetExplorer:

    def __init__(self, dataset):
        self.dataset = dataset

    def understand(self, verbose=True):
        """Unsupported"""
        logger.error("Unacceptable Data type provided. Type {} is "
                     "not supported".format(self.dataset.type))
        raise InvalidDatatypeException("Provided Data Type : {} not "
                                       "supported".format(
            self.dataset.type))

    def explore_univariate(self, verbose=True, to_excel=False,
                           validity_threshold=None,
                           output_path=None,
                           file_name=None,
                           bins=None):
        """Unsupported"""
        raise InvalidDatatypeException("Provided Data Type : {} not "
                                       "supported".format(self.dataset.type))

    def explore_multivariate(self, verbose=True, to_excel=False,
                             output_path=None,
                             file_name=None):
        """Unsupported"""
        raise InvalidDatatypeException("Provided Data Type : {} not "
                                       "supported".format(self.dataset.type))

    def explore_unstructured(self, verbose=True, to_excel=False):
        """Unsupported"""
        logger.warning(
            "Unsupported datatype provided to explore_unstructured")
        raise InvalidDatatypeException("Provided Data Type : {} not "
                                       "supported".format(
            self.dataset.type))
